# zabbix_openwrt

The original OpenWRT Zabbix template is for Zabbix 2.0 on pastebin. I've updated the template to get it to import into 4.4.
https://pastebin.com/nQdZM89w

I've updated my instance to 5.0 and am happy to report that this is still working. For other version compatibility, please let me know if it works or not.

Also, there is a dependance on a few official Zabbix templates. If you find yourself needing them, they are in the Zabbix Git repo: https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates
